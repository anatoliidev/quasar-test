# Quasar Test 

> Stack [Quasar Framework](https://github.com/quasarframework/quasar) and [Vue.js](https://github.com/vuejs/vue).

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ npx quasar dev

# build for production with minification
$ npx quasar build

# lint code
$ npx quasar lint
```
# Created by Anatolii 2019-03-17